-- upgrade-4.1.0.0.0-4.1.0.0.1.sql

SELECT acs_log__debug('/packages/intranet-trans-trados/sql/postgresql/upgrade/upgrade-4.1.0.0.0-4.1.0.0.1.sql','');

update im_component_plugins set package_name = 'intranet-trans-trados', title_tcl = 'lang::message::lookup "" intranet-translation.Company_Trados_Folder "Company Trados Folder"' where plugin_name = 'Intranet Company Trados Folder Component';

update im_component_plugins set package_name = 'intranet-trans-trados', title_tcl = 'lang::message::lookup "" intranet-trans-trados.Project_Trados_Folder "Project Trados Folder"' where plugin_name = 'Intranet Project Trados Folder Component';

delete from apm_parameters where package_key = 'intranet-trans-trados';

update apm_parameters set package_key = 'intranet-trans-trados' where parameter_name like 'Trados%' and section_name = 'trados';
	
create or replace function inline_0 ()
returns integer as $body$
declare
		v_package_id integer;
begin
	
	select package_id into v_package_id from apm_packages where package_key = 'intranet-trans-trados';

update apm_parameter_values set package_id = v_package_id where parameter_id in (select parameter_id from apm_parameters where package_key = 'intranet-trans-trados');

	return 0;

end;$body$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();
