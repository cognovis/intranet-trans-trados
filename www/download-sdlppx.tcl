# /packages/intranet-trans-trados/trados/download-sdlppx.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Purpose: Download all Source Files as PDFS
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2016-06-15
} {
    project_id
    freelancer_id
    task_type
}

# -----------------------------------------------------------
# Defaults
# -----------------------------------------------------------
set user_id [ad_maybe_redirect_for_registration]

    
set perm_p 0
# Check if the user has admin rights on the parent_id
# to allow freelancers to add sub-projects
im_project_permissions $user_id $project_id view read write admin
if {$admin} { set perm_p 1 }
    

# ---------------------------------------------------------------------
# Get some more information about the project
# ---------------------------------------------------------------------

im_trans_trados_project_info -project_id $project_id -array_name project_info


set project_dir $project_info(project_dir)
set trados_dir $project_info(trados_dir)
set project_nr $project_info(project_nr)

# Get the trans_tasks for the user
set trans_task_ids [db_list tasks "select task_id from im_trans_tasks where ${task_type}_id = :freelancer_id and project_id = :project_id"]

set sdlppx_file [im_trans_trados_create_package -trans_task_ids $trans_task_ids -task_type $task_type]

set filename [lindex [split $sdlppx_file "/"] end]

file rename -force $sdlppx_file "/tmp/$filename"

# Return the file
set outputheaders [ns_conn outputheaders]
ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"$filename\""

ns_returnfile 200 application/sdl "/tmp/$filename"

file delete -force "/tmp/$filename"