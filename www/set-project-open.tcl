# /packages/intranet-trans-trados/set-project-open.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Download an updated TM/TB and set the status to open
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2016-10-18
} {
    {project_id ""}
}

# -----------------------------------------------------------
# Defaults
# -----------------------------------------------------------
set user_id [ad_maybe_redirect_for_registration]

# Check if the user has admin rights on the parent_id
# to allow freelancers to add sub-projects
set perm_p 0
im_project_permissions $user_id $project_id view read write admin
if {$admin} { set perm_p 1 }
    
# Users with "add_projects" privilege can always create new projects...
if {[im_permission $user_id add_projects]} { set perm_p 1 } 
if {!$perm_p} { 
    ad_return_complaint "Insufficient Privileges" "
        <li>You don't have sufficient privileges to see this page."
    return
}

# ---------------------------------------------------------------------
# Get some more information about the project
# ---------------------------------------------------------------------

set project_query "
    select
        p.project_nr,
        p.source_language_id,
        company_id,
        final_company_id,
        subject_area_id,
        project_type_id
    from
        im_projects p
    where
        p.project_id=:project_id
"

if { ![db_0or1row projects_info_query $project_query] } {
    ad_return_complaint 1 "[_ intranet-translation.lt_Cant_find_the_project]"
    return
}

# ---------------------------------------------------------------
# Open the project
# ---------------------------------------------------------------

db_dml update_project "update im_projects set project_status_id = [im_project_status_open] where project_id = :project_id"

set start_timestamp [db_string start_date "select to_char(now(),'YYYY-MM-DD HH24:MI') from dual"]

im_translation_update_project_dates -project_id $project_id -start_timestamp $start_timestamp

# Update the quote

set quote_id [db_string quote "select cost_id from im_costs c, acs_objects o where cost_type_id = [im_cost_type_quote] and project_id = :project_id order by last_modified desc limit 1" -default ""]

if {$quote_id ne ""} {
    db_dml accept_quote "update im_costs set cost_status_id = [im_cost_status_accepted] where quote_id = :quote_id"
    db_foreach unaccepted_quote "select cost_id as declined_quote_id from im_costs where cost_type_id = [im_cost_type_quote] and cost_status_id = [im_cost_status_created] and project_id = :project_id and cost_id != :quote_id" {
        db_dml decline_quote "update im_costs set cost_status_id = [im_cost_status_filed] where quote_id = :declined_quote_id"
    }
}

im_project_audit -project_id $project_id -type_id $project_type_id -status_id [im_project_status_open] -action after_update

# ---------------------------------------------------------------
# Check if we have new files. 
# If yes, warn the user to update them in the project
# ---------------------------------------------------------------

if {[im_trans_trados_project_p -project_id $project_id]} {
    set new_files_p [im_trans_trados_new_file_p -project_id $project_id]
} else {
    set new_files_p 0
}
set project_url [export_vars -base "/intranet/projects/view" -url {project_id}]
if {!$new_files_p} {
    ad_returnredirect $project_url
} else {
    set new_file_url [export_vars -base "/intranet-trans-trados/update-tmbs-from-company" -url {project_id}]
}