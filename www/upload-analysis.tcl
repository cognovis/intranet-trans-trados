# /packages/intranet-translation/trados/download-sdlproj.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    Upload a Trados wordcount (.CSV) file and convert
    every line of it into an im_task for the Translation
    Workflow.
    The main work is done by "trados-import.tcl", so we
    basically only have to provide the trados file.

    @param project_id The parent project
    @param return_url Where to go after the work is done?
    @param wordcount_application Allows to upload data from
           various Translation Memories
    @param tm_type_id Really necessary? Not used yet, 
           because the TM type is given from the wordcount_app
    @param task_type_id determines the task type
    @param upload_file The filename to be uploaded - according
           to AOLServer conventions

} {
    project_id:integer
    return_url
    { wordcount_application "trados" }
    { task_type_id 0 }
    { target_language_id "" }
    {quote_p "0"}
    upload_file
} 

# ---------------------------------------------------------------------
# Defaults & Security
# ---------------------------------------------------------------------

set user_id [ad_maybe_redirect_for_registration]
im_project_permissions $user_id $project_id view read write admin
if {!$write} {
    ad_return_complaint 1 "<li>[_ intranet-translation.lt_You_have_insufficient_3]"
    return
}

# ---------------------------------------------------------------
# Analyse the trados XML File and create the tasks
# ---------------------------------------------------------------
    
if {[exists_and_not_null upload_file]} {
        
    # Read the file
    set wordcount_file [ns_queryget upload_file.tmpfile]
    
    # ---------------------------------------------------------------------
    # Get the file and deal with Unicode encoding...
    # ---------------------------------------------------------------------
        
    # Analyse it
    im_trans_trados_create_tasks \
        -project_id $project_id \
        -trados_analysis_xml $wordcount_file \
        -target_language_id $target_language_id
        
    # Update the processing time
    set days [im_translation_processing_time -project_id $project_id -include_pm -update]
    
    # Find the latest quote and replace it if wanted
    if {$quote_p} {
    	set invoice_id [db_string quote "select max(cost_id) from im_costs where project_id = :project_id and cost_type_id = [im_cost_type_quote]" -default ""]
    	if {$invoice_id ne ""} {
    	    im_trans_invoice_create_from_tasks -project_id $project_id -invoice_id $invoice_id
    	} else {
    	    # No quote to replace
    	    set invoice_id [im_trans_invoice_create_from_tasks -project_id $project_id]
    	}
    	ad_returnredirect [export_vars -base "/intranet-invoices/view" -url {invoice_id}]
    } 
}

# No quote or no upload file, redirect to the trans task page 
ad_returnredirect "/intranet-translation/trans-tasks/task-list?[export_url_vars project_id return_url]"

