# /packages/intranet-trans-trados/update-tmbs-from-folder.tcl
#
# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Purpose: Update a translation project from the trados windows folder
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2011-08-05
} {
    project_id:integer
    {analysis_p "0"}
    {quote_p "0"}
}

# -----------------------------------------------------------
# Defaults
# -----------------------------------------------------------
set user_id [ad_maybe_redirect_for_registration]

    
set perm_p 0
set quote_id ""

# Check if the user has admin rights on the parent_id
# to allow freelancers to add sub-projects
im_project_permissions $user_id $project_id view read write admin
if {$admin} { set perm_p 1 }
    
# Users with "add_projects" privilege can always create new projects...
if {[im_permission $user_id add_projects]} { set perm_p 1 } 
if {!$perm_p} { 
    ad_return_complaint "Insufficient Privileges" "
        <li>You don't have sufficient privileges to see this page."
    return
}


set return_url [export_vars -base "/intranet/projects/view" {project_id}]
set project_url $return_url

db_1row project_query "
    select
        p.project_nr,
        p.source_language_id,
        p.project_type_id,
        company_id,
        final_company_id,
        subject_area_id
    from
        im_projects p
    where
        p.project_id=:project_id
"
im_trans_trados_project_info -project_id $project_id -array_name project_info

set project_dir $project_info(project_dir)
set trados_dir $project_info(trados_dir)


set source_language [im_name_from_id $source_language_id]

# ---------------------------------------------------------------
# Update the TMs
# ---------------------------------------------------------------

if {[im_trans_trados_configured_p]} {
    foreach target_language_id [im_target_language_ids $project_id] {
	# Get the expected TMs for the target language

	set tm_paths [im_trans_trados_get_trans_memory_file \
            -company_id $company_id \
            -final_company_id $final_company_id \
            -source_language_id $source_language_id \
            -target_language_id $target_language_id \
            -subject_area_id $subject_area_id]

	foreach file_path $tm_paths {
	    # Check if the file exists                                
	    set filename "[lindex [split [file rootname $file_path] "/"] end].sdltm"
	    if {[file exists "${trados_dir}/TM/$filename"]} {
		ns_log Notice "Updating TM $file_path from project $project_nr"
		# Upload the new TM
		im_trans_trados_update_tm \
		    -current_tm $file_path \
		    -new_tm ${trados_dir}/TM/$filename \
		    -project_id $project_id
	    }
	} 
    }

    if {"${project_dir}/Trados" ne $trados_dir} {
	im_filestorage_rsync -source_path $trados_dir -target_path "${project_dir}/Trados"
    }
}

ad_returnredirect $project_url
